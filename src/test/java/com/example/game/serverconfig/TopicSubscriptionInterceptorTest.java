package com.example.game.serverconfig;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;

import com.example.game.model.GamePlayerData;

class TopicSubscriptionInterceptorTest {

	@InjectMocks
	TopicSubscriptionInterceptor interceptor;
	
	@Mock 
	GamePlayerData gpc;
	
	Message<?> message;
	StompHeaderAccessor accessor;
	
	final String DESTINATION = "/topic/ABC123";

	@BeforeEach
	<T> void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		
		message = new Message<T>() {
			@Override
			public MessageHeaders getHeaders() {
				
				Map<String, Object> headerMap = new HashMap<>();
				headerMap.put("stompCommand", StompCommand.SUBSCRIBE);
				MessageHeaders header = new MessageHeaders(headerMap);
				
				return header;
			}

			@Override
			public T getPayload() {
				return null;
			}
		};
	}

	@Test
	void whenPlayerSubscribes_thenUpdatePlayerCountMap() {
		
		StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
		
		assertEquals(StompCommand.SUBSCRIBE, accessor.getCommand());
		
		gpc.getPlayerCount().merge(DESTINATION, 1, Integer::sum);
		
		assertNotEquals(0, gpc.getPlayerCount().get(DESTINATION));
	}

}
