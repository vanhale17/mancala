package com.example.game.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.example.game.model.GameData;
import com.example.game.model.GamePlayerData;
import com.example.game.model.PlayerData;

class GameServiceImplTest {

	@InjectMocks
	GameServiceImpl gameService;
	
	@Mock
	GamePlayerData gpc;
	
	GameData gameData;
	
	final int SMALL_PIT_SIZE = 6;
	final int PLAYER_COUNT = 2;
	
	final String GAME_ID = "ABC123";
	final String PLAYER_1_NAME = "Player1";
	final String PLAYER_2_NAME = "Player2";
	final String ACTION_TURN = "TURN";
	final String ACTION_END_TURN = "END_TURN";
	final String ACTION_END_GAME = "END_GAME";
	
	final int PLAYER_1_BIG_PIT = 17;
	final int[] PLAYER_1_SMALL_PIT_DEFAULT = {6, 6 ,6 ,6 ,6, 6};
	
	final int[] PLAYER_2_SMALL_PIT_DEFAULT = {6, 6 ,6 ,6 ,6, 6};
	final int PLAYER_2_BIG_PIT = 10;
	
	final int SEL_PIT_END_GAME = 6;
	final int[] PLAYER_1_SMALL_PIT_END_GAME = {0, 0 ,0 ,0 ,0, 1};
	
	final int SEL_PIT_END_TURN_IN_SMALL_PIT = 1;
	final int[] PLAYER_1_SMALL_PIT_END_TURN_ONE_STONE = {1, 0 ,6 ,6 ,6, 6};
	
	final int SEL_PIT_END_TURN = 2;
	final int SEL_PIT_END_TURN_IN_BIT_PIT = 1;
	
	final String TOPIC = "/topic";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.openMocks(this);
		
		String destination = TOPIC + "/" + GAME_ID;
		gpc.getPlayerCount().merge(destination, 2, Integer::sum);
		
		gameData = new GameData();
		gameData.setGameId(GAME_ID);
		gameData.setAction(ACTION_TURN);
		gameData.setSender(PLAYER_1_NAME);
		
		List<PlayerData> players = new ArrayList<>();
		
		PlayerData player1Data = new PlayerData();
		player1Data.setPlayerName(PLAYER_1_NAME);
		player1Data.setBigPit(PLAYER_1_BIG_PIT);
		
		PlayerData player2Data = new PlayerData();
		player2Data.setPlayerName(PLAYER_2_NAME);
		player2Data.setBigPit(PLAYER_2_BIG_PIT);
		player2Data.setSmallPit(PLAYER_2_SMALL_PIT_DEFAULT);
		
		players.add(player1Data);
		players.add(player2Data);
		
		gameData.setPlayerData(players);
			
	}

	@Test
	void whenPlayerHasZeroStonesInSmallPit_thenEndGame() {
		gameData.setSelectedPit(SEL_PIT_END_GAME);
		PlayerData player1Data = getPlayerDataByName(gameData, PLAYER_1_NAME);
		PlayerData player2Data = getPlayerDataByName(gameData, PLAYER_2_NAME);
		
		assertNotNull(player1Data);
		
		int p1bigPitPrevStoneCount = player1Data.getBigPit();
		int p2bigPitPrevStoneCount = player2Data.getBigPit();
		
		player1Data.setSmallPit(PLAYER_1_SMALL_PIT_END_GAME);
		gameService.computeDistribrution(gameData);
		
		int[] expectedSmallPit = new int[SMALL_PIT_SIZE];
		Arrays.fill(expectedSmallPit, 0);
		
		assertEquals(ACTION_END_GAME, gameData.getAction());
		assertArrayEquals(expectedSmallPit, player1Data.getSmallPit());
		assertArrayEquals(expectedSmallPit, player2Data.getSmallPit());
		assertTrue(p1bigPitPrevStoneCount < player1Data.getBigPit());
		assertTrue(p2bigPitPrevStoneCount < player2Data.getBigPit());
	}
	
	@Test
	void whenPlayerLastStoneLandsOnBigPit_thenPlayerGetsAnotherTurn() {
		gameData.setSelectedPit(SEL_PIT_END_TURN_IN_BIT_PIT);
		PlayerData player1Data = getPlayerDataByName(gameData, PLAYER_1_NAME);
		
		assertNotNull(player1Data);
		
		int bigPitPrevStoneCount = player1Data.getBigPit();
		
		player1Data.setSmallPit(PLAYER_1_SMALL_PIT_DEFAULT);
		gameService.computeDistribrution(gameData);
		
		assertEquals(ACTION_TURN, gameData.getAction());
		assertTrue(player1Data.getBigPit() > bigPitPrevStoneCount);
		
	}
	
	@Test
	void whenPlayerLastStoneLandsOnNonEmptySmallPit_thenEndPlayerTurn() {
		gameData.setSelectedPit(SEL_PIT_END_TURN);
		PlayerData player1Data = getPlayerDataByName(gameData, PLAYER_1_NAME);
		
		assertNotNull(player1Data);
		
		player1Data.setSmallPit(PLAYER_1_SMALL_PIT_DEFAULT);
		gameService.computeDistribrution(gameData);
		
		assertEquals(ACTION_END_TURN, gameData.getAction());
		
	}
	
	@Test
	void whenPlayerLastStoneLandsOnEmptyPit_thenGetOtherPlayerStoneInOppositeSmallPit_thenEndPlayerTurn() {
		gameData.setSelectedPit(SEL_PIT_END_TURN_IN_SMALL_PIT);
		PlayerData player1Data = getPlayerDataByName(gameData, PLAYER_1_NAME);
		PlayerData player2Data = getPlayerDataByName(gameData, PLAYER_2_NAME);
		
		assertNotNull(player1Data);
		
		int bigPitPrevStoneCount = player1Data.getBigPit();
		player1Data.setSmallPit(PLAYER_1_SMALL_PIT_END_TURN_ONE_STONE);
		gameService.computeDistribrution(gameData);
		
		assertEquals(ACTION_END_TURN, gameData.getAction());
		assertTrue(player1Data.getBigPit() >  bigPitPrevStoneCount);
		assertEquals(0, player2Data.getSmallPit()[4]);
		assertEquals(0, player1Data.getSmallPit()[1]);
		
	}
	
	private PlayerData getPlayerDataByName(GameData data, String playerName) {
		return gameData.getPlayerData().stream()
				.filter(i -> playerName.equals(i.getPlayerName()))
				.findFirst()
				.orElse(null);
	}
	
}
