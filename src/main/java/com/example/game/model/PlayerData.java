package com.example.game.model;

import lombok.Getter;
import lombok.Setter;

public class PlayerData {
	
	@Getter
	@Setter
	private String playerName;
	
	@Getter
	@Setter
	private int[] smallPit;
	
	@Getter
	@Setter
	private int bigPit;
}
