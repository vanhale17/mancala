package com.example.game.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class GameConfig {
	
	@Getter
	private final int minPlayers;
	
	@Getter
	private final int maxPlayers;

	@Getter
	private final int defaultSmallPit;
	
	@Getter
	private final int defaultStones;
	
    public GameConfig(@Value("${min.players}") int minPlayers, @Value(("${max.players}")) int maxPlayers,
    		@Value("${default.small.pit}") int defaultSmallPit, @Value(("${default.stones}")) int defaultStones) {
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.defaultSmallPit = defaultSmallPit;
        this.defaultStones = defaultStones;
    }
}
