package com.example.game.model;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
public class GamePlayerData {

	@Getter
	@Setter
	private Map<String, Integer> playerCount = new HashMap<>();
	
	public GamePlayerData() {
		
	}
}
