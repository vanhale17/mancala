package com.example.game.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class GameData {

	@Getter
	@Setter
	private String gameId;
	
	@Getter
	@Setter
	private String action;
	
	@Getter
	@Setter
	private String sender;
	
	@Getter
	@Setter
	private int selectedPit;
	
	@Getter
	@Setter
	private List<PlayerData> playerData;
}
