package com.example.game.service;

import com.example.game.model.GameData;

public interface GameService {
	
	public void computeDistribrution(GameData data);
	
}
