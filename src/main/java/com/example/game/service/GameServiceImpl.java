package com.example.game.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.game.model.GameData;
import com.example.game.model.GamePlayerData;
import com.example.game.model.PlayerData;

@Service
public class GameServiceImpl implements GameService{

	@Autowired
	private GamePlayerData gpc;
	
	// Read from props
	final int SMALL_PIT_SIZE = 6;
	final int PLAYER_COUNT = 2;
	
	public void computeDistribrution(GameData data) {
		// Get players and stones to distribute
		int selectedPit = data.getSelectedPit();
		
		List<String> playerNames = data.getPlayerData().stream().map(i -> i.getPlayerName()).collect(Collectors.toList()); 
		PlayerData currentPlayer = getPlayerData(data, playerNames.get(0));
		PlayerData otherPlayer = getPlayerData(data, playerNames.get(1));
		
		int distArraySize = 0;
		int selectedPitIndex = selectedPit-1;
		int bigPitIndex = SMALL_PIT_SIZE;
		
		if (currentPlayer != null && otherPlayer != null) {
			int stonesToDistribute = currentPlayer.getSmallPit()[selectedPitIndex];

			int[] distributionArray = merge(currentPlayer.getSmallPit(), new int[] { currentPlayer.getBigPit() }, otherPlayer.getSmallPit());

			distArraySize = distributionArray.length;
			int startIndex = data.getSelectedPit();
			int actualIndex = 0;
			actualIndex += startIndex;

			// Distribute stones
			distributionArray[actualIndex-1] = 0;
			for (int distCtr = 0; distCtr < stonesToDistribute; distCtr++) {

				distributionArray[actualIndex] += 1;

				if ((actualIndex + 1) < distArraySize) { 
					actualIndex += 1; 
				} else {
					actualIndex = 0;
				}
				
			}
			
			int lastStoneIndex = actualIndex == 0 ? (distArraySize - 1): (actualIndex - 1);
			boolean canGetOtherPlayerStone = (lastStoneIndex < bigPitIndex) && (distributionArray[lastStoneIndex] == 1);
			boolean lastStoneInBigPit = lastStoneIndex == bigPitIndex;
			
			if(canGetOtherPlayerStone) {
				int otherPlayerPitIndex = (distArraySize - 1) - lastStoneIndex;
				int totalStonesToGet = distributionArray[otherPlayerPitIndex] + 1;
				
				distributionArray[lastStoneIndex] = 0;
				distributionArray[otherPlayerPitIndex] = 0;
				distributionArray[bigPitIndex] += totalStonesToGet;
				
				data.setAction("END_TURN");
				
			} else if (!lastStoneInBigPit) {
				data.setAction("END_TURN");
			}
			
			updateSmallPit(currentPlayer,distributionArray, 0, bigPitIndex);
			updateBigpit(currentPlayer, distributionArray[bigPitIndex]);
			
			updateSmallPit(otherPlayer,distributionArray, bigPitIndex+1, distArraySize);
			
			if(isGameEnding(data)) {
				
				data.getPlayerData().forEach(this::emptySmallPit);
				data.setAction("END_GAME");
				
				String destination = "/topic/" + data.getGameId();
				gpc.getPlayerCount().remove(destination);
			}

		}
		
	}
	
	private PlayerData getPlayerData(GameData data, String sender) {
		
		PlayerData thistPlayerData = data.getPlayerData().stream()
				.filter(player -> player.getPlayerName().equals(sender))
				.reduce((a, b) -> b).orElse(null);
		
		return thistPlayerData;
		
	}
	
	private void updateSmallPit(PlayerData playerData, int[] distributionArray, int startIndex, int smallPitCount) {
		playerData.setSmallPit(IntStream.range(startIndex, smallPitCount).map(i -> distributionArray[i]).toArray());
	}
	
	private void updateBigpit(PlayerData playerData, int newVal) {
		playerData.setBigPit(newVal);
	}
	
	private boolean isGameEnding(GameData data) {	
		return !data.getPlayerData().stream()
				.filter(i -> isSmallPitEmpty(i.getSmallPit()))
				.findFirst()
				.isEmpty();

	}
	
	private boolean isSmallPitEmpty(int[] smallPit) {
		
		for(int stoneCount: smallPit) {
			if(stoneCount != 0) {
				return false;
			}
		}
		
		return true;
		
	}
	
	private void emptySmallPit(PlayerData playerData) {
		int bigPit = playerData.getBigPit() + IntStream.of(playerData.getSmallPit()).reduce(0, Integer::sum);
		playerData.setBigPit(bigPit);
		
		int[] zeroArray = new int[SMALL_PIT_SIZE];
		Arrays.fill(zeroArray, 0);
		
		playerData.setSmallPit(zeroArray);
	}
	
	private int[] merge(int[]... intArrays) {
		return Arrays.stream(intArrays).flatMapToInt(i -> Arrays.stream(i)).toArray();
	}
}
