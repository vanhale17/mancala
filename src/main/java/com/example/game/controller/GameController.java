package com.example.game.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.example.game.model.GameConfig;
import com.example.game.model.GameData;
import com.example.game.service.GameServiceImpl;

@Controller
public class GameController {
	
	@Autowired
	GameServiceImpl gameservice;
	
	@Autowired
	SimpMessagingTemplate simpMessagingTemplate;

	@Autowired
	GameConfig config;
	
	@MessageMapping("/selectPit")
	public void distributeStones(@Payload GameData gameData) throws Exception {

		if(isDataValid(gameData)) {
			gameservice.computeDistribrution(gameData);
			
			String destination = "/topic/" + gameData.getGameId();
			simpMessagingTemplate.convertAndSend(destination, gameData);
		} else {
			throw new Exception ("Unable to send message to server.");
		}
	}
	
	private boolean isDataValid(GameData gameData) {
		int smallPitCount = config.getDefaultSmallPit();
		boolean isValid = true;
		
		if(gameData == null || gameData.getPlayerData() == null
				|| gameData.getAction() == null || gameData.getSender() == null
				|| gameData.getPlayerData().size() != config.getMaxPlayers()
				|| !gameData.getAction().equals("TURN")
				|| gameData.getSelectedPit() <= 0 || gameData.getSelectedPit() > smallPitCount) {
				
			isValid = false;
			
		} else {
			isValid = gameData.getPlayerData().stream().filter(i -> {
				int[] zeroArray = new int[smallPitCount];
				Arrays.fill(zeroArray, 0);
				
				return (i.getPlayerName() == null || i.getSmallPit() == null
						|| i.getSmallPit() == zeroArray);
				
			}).findAny().isEmpty();
		}
		
		
		return isValid;
		
		
	}

}
