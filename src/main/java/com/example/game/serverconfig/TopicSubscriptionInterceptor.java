package com.example.game.serverconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;

import com.example.game.model.GameConfig;
import com.example.game.model.GamePlayerData;

@Component
public class TopicSubscriptionInterceptor implements ChannelInterceptor {

	@Autowired
	GameConfig config;
	
	@Autowired
	private GamePlayerData gpc;

	@Override
	public Message<?> preSend(Message<?> message, MessageChannel channel) throws IllegalArgumentException {

		StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
		String destination = accessor.getDestination();

		if (StompCommand.SUBSCRIBE.equals(accessor.getCommand())) {
			int playerCount = gpc.getPlayerCount().getOrDefault(destination, 0);

			if (playerCount >= config.getMaxPlayers()) {
				throw new IllegalArgumentException("Game has reached the maximum number of players.");
			} else {
				gpc.getPlayerCount().merge(destination, 1, Integer::sum);
			}

		}

		return message;
	}
}
