package com.example.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MancalaApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/game");
		SpringApplication.run(MancalaApplication.class, args);
	}

}
