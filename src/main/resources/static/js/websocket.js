let stompClient = null;
let gameId = null;
let destination = null;
let myName = null;
let sender = null;
var otherPlayerName = null;
let $txtGameId = null;
let $btnPlay = null;
let $errMsg = null;

$(function() {
    $txtGameId = $('#txt-game-id');
	$btnPlay = $('#btn-play-game');
	$playerMsg = $('#div-player-msg');
});


function joinGame() {
	myName = $('#txt-player-name').val();
	if(myName) {
		gameId = $('#txt-game-id').val()
		
		if(gameId === "") {
			gameId = generateGameId();	
		} else {
			gameId = $('#txt-game-id').val().trim();
		}
		
	    $txtGameId.prop( "disabled", true );
	    $btnPlay.prop( "disabled", true );
	    $txtGameId.val(gameId);
	    
	    let socket = new SockJS('./ws');
	        stompClient = Stomp.over(socket);
	        stompClient.connect({}, onConnected, onConnError);
    } else {
		setPlayerMsg("Hey, what's your name?");
	}
}

function onConnected() {
	destination = "/topic/" + gameId;
	stompClient.subscribe(destination, onMessageReceived);
	sendJoin();
	initializePitsAndStones();
	
}

function onMessageReceived(payload){
	
	if(payload) {
		myName = $('#txt-player-name').val();
		let gameData = JSON.parse(payload.body);
		let action = gameData.action;
		let sender = gameData.sender;
		let playerData = gameData.playerData;
		
		if(action === 'JOIN'){
			if(sender === myName) {
				setPlayerMsg("Challenge someone with this Game ID: " + gameId);
			} else {
				otherPlayerName = sender;
				setPlayerMsg(sender + ' has joined! You pick first.');
				sendAckJoin();
				startTurn();
			}
		} else if(action === 'ACK' && sender !== myName) {
			otherPlayerName = sender;
			setPlayerMsg(otherPlayerName + ' has joined!');
			endTurn();
		} else if(action === 'TURN') {
			// Needs improvement
			populatePitsAndStones(playerData, sender);
			if(sender === myName) {
				setPlayerMsg('Pick some stones again!');
			} else {
				closePlayerMsg();
			}
		} else if(action === 'END_TURN') {
			populatePitsAndStones(playerData, sender);
			if(sender === myName) {
				setPlayerMsg('Your turn has ended!');
				endTurn();
			} else {
				setPlayerMsg('Pick your stones!')
				startTurn();
			}
		} else if(action === 'END_GAME') {
			populatePitsAndStones(playerData, sender);
			setPlayerMsg("Game has ended! " + gameWinner() + " won!");
			endTurn();
		}
	}
}

function onConnError() {
	stompClient.disconnect();
    $txtGameId.val('');
    $txtGameId.prop( "disabled", false );
    $btnPlay.prop( "disabled", false );
    setPlayerMsg("Something went horribly wrong. Please try again.");
}

function sendJoin() {
	myName = $('#txt-player-name').val();
	destination = "/topic/" + gameId;
	stompClient.send(destination, {}, JSON.stringify(
			{
				'gameId' : gameId,
				'sender' : myName,
				'action' : 'JOIN'
			}
		)
	);
}

function sendAckJoin() {
	myName = $('#txt-player-name').val();
	destination = "/topic/" + gameId;
	stompClient.send(destination, {}, JSON.stringify(
			{
				'gameId' : gameId,
				'sender' : myName,
				'action' : 'ACK'
			}
		)
	);
}

function sendPit(selectedPit) {
	
	closePlayerMsg();
	let pitStones = $('#p1-small-pit'+selectedPit).text();
	
	if(pitStones !== '0') {
		myName = $('#txt-player-name').val();
		destination = "/game/selectPit";
		stompClient.send(destination, {}, JSON.stringify(
				{
					'gameId' : gameId,
					'sender' : myName,
					'action' : 'TURN',
					'selectedPit' : selectedPit,
					'playerData' : [{
						'playerName' : myName,
						'smallPit' : getPlayerSmallPit(1),
						'bigPit' : getPlayerBigPit(1) },
						{ 'playerName' : otherPlayerName,
						'smallPit' : getPlayerSmallPit(2),
						'bigPit' : getPlayerBigPit(2) }
					
					]
				}
			)
		);
	} else {
		setPlayerMsg('You cannot do that. Pick some stones!');
	}
}

const generateGameId = (length = 6) => {
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    let str = '';
    for (let i = 0; i < length; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return str;
}