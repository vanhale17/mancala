const smallPitCount = 6;
const smallPitInitStones = 6;
const bigPitInitStones = 0;
const playerCount = 2;


var player1SmallPit;
	var player1BigPit;
	
	var player2SmallPit;
	var player2BigPit;
	
	
function initGameBoard() {	
	var $gameBoard = $('#div-game-board');
	
	$gameBoard.append('<div class="p2-big-pit pit-shadow center" id="p2-big-pit"></div>');
	$gameBoard.append('<table class="center" id="tbl-small-pit"></table>');
	
	$smallPitTable = $('#tbl-small-pit');
	$smallPitTbody = $('<tbody>');
	
	// Create small pit for player 2
	var $smallPitRowP2 = $('<tr id="p2-small-pit-row">');
	var smallPitColP2 = '';
	
	for(let smallPitCtr = smallPitCount; smallPitCtr >= 1; smallPitCtr--) {
		smallPitColP2 = smallPitColP2 + '<td class="p2-small-pit pit-shadow" id="p2-small-pit' + smallPitCtr + '"></td>';
	}
	
	$smallPitRowP2.append(smallPitColP2);
	
	// Create small pit for player 1
	var $smallPitRowP1 = $('<tr id="p1-small-pit-row">');
	var smallPitColP1 = '';
	
	for(let smallPitCtr = 1; smallPitCtr <= smallPitCount; smallPitCtr++) {
		smallPitColP1 = smallPitColP1 + '<td class="p1-small-pit pit-shadow" id="p1-small-pit' + smallPitCtr + '"></td>';
	}
	
	$smallPitRowP1.append(smallPitColP1);
	
	// Create the small pit table
	$smallPitTbody.append($smallPitRowP2);
	$smallPitTbody.append($smallPitRowP1);
	$smallPitTable.append($smallPitTbody);
	
	$gameBoard.append('<div class="p1-big-pit pit-shadow center" id="p1-big-pit"></div>');
	
}

function initializePitsAndStones() {
	for(let playerCtr = 1; playerCtr <= playerCount; playerCtr++) {
		for(let smallPitCtr = 1; smallPitCtr <= smallPitCount; smallPitCtr++) {
			$('#p' + playerCtr + '-small-pit' + smallPitCtr).text(smallPitInitStones);
		}
		
		$('#p'+ playerCtr + '-big-pit').text(bigPitInitStones);
	}

}

function getPlayerSmallPit(playerNum) {
	let smallPit = [];
	for(let smallPitCtr = 1; smallPitCtr <= smallPitCount; smallPitCtr++) {
		smallPit.push($('#p' + playerNum + '-small-pit' + smallPitCtr).text());
	}
	
	return smallPit;
}

function getPlayerBigPit(playerNum) {
	return $('#p'+ playerNum + '-big-pit').text();
}

function gameWinner() {
	if (getPlayerBigPit(1) > getPlayerBigPit(2)) {
		return myName;
	} else {
		return otherPlayerName;
	}
}

function populatePitsAndStones(playerData, sender) {
	myName = $('#txt-player-name').val();
	player1SmallPit = playerData.filter(i => i.playerName === myName).map(i => i.smallPit);
	player1BigPit = playerData.filter(i => i.playerName === myName).map(i => i.bigPit);
	
	player2SmallPit = playerData.filter(i => i.playerName !== myName).map(i => i.smallPit);
	player2BigPit = playerData.filter(i => i.playerName !== myName).map(i => i.bigPit);
	
	if(sender === myName) {
		populatePlayerPits(1, player1SmallPit, player1BigPit);
		populatePlayerPits(2, player2SmallPit, player2BigPit);
	} else {
		populatePlayerPits(2, player2SmallPit, player2BigPit);
		populatePlayerPits(1, player1SmallPit, player1BigPit);
	}
}

const sleep = (ms) => new Promise((res) => setTimeout(res, ms));

function populatePlayerPits(playerNum, smallPit, bigPit) {
	for(let smallPitCtr = 0; smallPitCtr < smallPit[0].length; smallPitCtr++) {
		let elementCtr = smallPitCtr+1;
		$('#p' + playerNum + '-small-pit' + elementCtr).text(smallPit[0][smallPitCtr]);
	}
	
	$('#p' + playerNum + '-big-pit').text(bigPit);
}

function endTurn() {
	$('td').unbind("click");
}

function startTurn() {
	for(let i=1; i <= smallPitCount; i++) {
		$( "#p1-small-pit"+i ).click(function() { sendPit(i) });
	}
}

function setPlayerMsg(msg) {
	$('#div-player-msg').text(msg);
}

function closePlayerMsg() {
	$('#div-player-msg').text('');
}

function showTurnDialogMsg() {
	$('#dlg-player-msg').show();
}

async function showHowTo() {
	setPlayerMsg("Introduce yourself.");
	await sleep(1700);
	setPlayerMsg("Create or join a with Game ID.");
	await sleep(1900);
	setPlayerMsg("Start playing.");
	await sleep(1700);
	closePlayerMsg()
}

$(function() {
	initGameBoard();
	closePlayerMsg();
});